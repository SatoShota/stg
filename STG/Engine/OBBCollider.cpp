#include "OBBCollider.h"

OBBCollider::OBBCollider(IGameObject* owner, D3DXVECTOR3 boxSize, D3DXVECTOR3 center)
{
	//持ち主の登録
	owner_ = owner;

	//OBBの大きさ
	boxSize_ = boxSize;

	//OBBの各辺の半分の長さ
	boxHalfSize_ = D3DXVECTOR3(boxSize_.x / 2.0f, boxSize_.y / 2.0f, boxSize_.z  / 2.0f);

	//OBBの中心
	center_ = center;

	//それぞれのもとの値をセット
	stdVecX = D3DXVECTOR3(1, 0, 0);
	stdVecY = D3DXVECTOR3(0, 1, 0);
	stdVecZ = D3DXVECTOR3(0, 0, 1);

	shape_ = OBB;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, boxSize_.x, boxSize_.y, boxSize_.z, &pMesh_, 0);
#endif
}


OBBCollider::~OBBCollider()
{
}

bool OBBCollider::IsHit(Collider * pTarget)
{
	//あたり判定により処理を変更
	switch (pTarget->shape_)
	{
	case SPHERE:return IsHitOBBAndSphere(this, (SphereCollider*)pTarget);

	}

	return false;
}

float OBBCollider::GetBoxHalfSize(unsigned int vecDir)
{
	switch (vecDir)
	{
	case 1:return boxHalfSize_.x;
		break;

	case 2:return boxHalfSize_.y;
		break;

	case 3:return boxHalfSize_.z;
		break;

	default: return 0.0f;
	}
}

D3DXVECTOR3 OBBCollider::GetStdVec(unsigned int stdVec)
{
	switch (stdVec)
	{
	case 1:return stdVecX;
		break;

	case 2:return stdVecY;
		break;

	case 3:return stdVecZ;
		break;
		
	default: return D3DXVECTOR3(0,0,0);
	}
}

void OBBCollider::SetStdVec(D3DXVECTOR3 & stdVec,unsigned int num)
{
	switch (num)
	{
	case 1: stdVecX = stdVec;
		break;

	case 2: stdVecY = stdVec;
		break;

	case 3: stdVecZ = stdVec;
		break;

	default: stdVec = D3DXVECTOR3(0,0,0);
	}
}

