#pragma once
#include "IGameObject.h"

//シーンの番号
enum SCENE_ID
{
	SCENE_ID_TITLE,	
	SCENE_ID_PLAY,
	SCENE_ID_RESULT
};

//シーンを管理するクラス
class SceneManager : public IGameObject
{
	//現在のシーン
	static SCENE_ID currentSceneID_;

	//次のシーン
	static SCENE_ID nextSceneID_;

	//現在のシーンのポインタ
	static IGameObject* pCurrentScene_;

public:

	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//次のシーンをきめる関数
	//引数	:	次のシーンのID
	//戻り値	:	なし
	static void ChangeScene(SCENE_ID next);

	//現在のシーンをもらう関数
	//引数	:	なし
	//戻り値	:	現在のシーン
	static IGameObject* GetCurrentScene();
};
