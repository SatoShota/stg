#pragma once
#include "Collider.h"

class OBBCollider: public Collider
{
	friend class Collider;

	//箱の各辺の大きさ
	D3DXVECTOR3 boxSize_;

	//箱の各辺の半分の大きさ
	D3DXVECTOR3 boxHalfSize_;
	
	//X方向の基準となるベクトル
	D3DXVECTOR3 stdVecX;

	//y方向の基準となるベクトル
	D3DXVECTOR3 stdVecY;

	//z方向の基準となるベクトル
	D3DXVECTOR3 stdVecZ;

public:
	//引数	:	持ち主、ボックスの大きさ、ボックスの中心
	OBBCollider(IGameObject* owner,  D3DXVECTOR3 boxSize, D3DXVECTOR3 center);
	~OBBCollider();

	//当たり判定
	//引数	:	相手のあたり判定
	//戻り値	:	当たったかどうか
	bool IsHit(Collider* pTarget) override;

	//ボックスの各辺の半分の長さを返します
	//引数	:	ほしい辺の情報 1,x 2,y 3,z,
	//戻り値	:	各辺の長さ、不正な値がきた場合０を返します
	float GetBoxHalfSize(unsigned int vecDir);

	//ボックスの各軸の基準となるベクトルを返します
	//引数	:	ほしいベクトルの情報1,x 2,y 3,z　を返します,
	//戻り値	:	各ベクトル
	D3DXVECTOR3 GetStdVec(unsigned int stdVec);

	//基準となるベクトルの設定するために使用
	//引数	:	基準ベクトル、XYZのいずれか（1=x,2=y,3=x);
	//戻り値	:	なし
	void SetStdVec(D3DXVECTOR3 &stdVec, unsigned int num);
};

