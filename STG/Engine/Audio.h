#pragma once
#include <xact3.h>
#include <string>
#include <assert.h>

namespace Audio
{
	//Audioの初期化
	//引数:なし
	//戻り値: なし
	void Initialize(void);

	//WaveBankの読み込み
	//引数 : ファイル名
	//戻り値 : なし
	void WaveBankLoad(std::string fileName);

	//SoundBankの読み込み
	//引数 : ファイル名
	//戻り値 : なし
	void SoundBankLoad(std::string fileName);

	//登録されたキューの再生
	//引数: キュー名
	//戻り値:なし
	void Play(std::string cueName);

	//登録されたキューの停止
	//引数: キュー名
	//戻り値:なし
	void Stop(std::string cueName);

	void Release();

}
