#include "Collider.h"
#include "Direct3D.h"
#include "SphereCollider.h"
#include "PlaneCollider.h"
#include "AABBCollider.h"
#include "OBBCollider.h"

Collider::Collider()
{
}


Collider::~Collider()
{
}

bool Collider::IsHitSphereAndSphere(SphereCollider * sphere1, SphereCollider * sphere2)
{
	//自分の球の中心と相手の球の中心との距離を取得（ベクトル）
	D3DXVECTOR3 vecLength = (sphere1->center_ + sphere1->owner_->GetPosition())
		- (sphere2->center_ + sphere2->owner_->GetPosition());

	//↑求めたベクトルを距離に変換
	float length = D3DXVec3Length(&vecLength);

	//二つの球との距離が二つの球の半径の合計より小さいなら真
	if (length <= (sphere1->radius_ + sphere2->radius_))
	{
		return true;
	}

	return false;
}

bool Collider::IsHitPlaneAndSphere(PlaneCollider * plane, SphereCollider * sphere)
{
	float IKD_EPSIRON = 0.00001f; // 誤差

	//時間
	float time;

	//衝突位置
	D3DXVECTOR3 collisionPos;

	//平面の上の一点から現在位置へのベクトル
	D3DXVECTOR3 planeToSphereVec = (sphere->prePos_) - (plane->owner_->GetPosition());

	//現在の位置から予定位置までのベクトル
	D3DXVECTOR3 nowToPlanSphereVec = (sphere->center_ + sphere->owner_->GetPosition()) - (sphere->prePos_);

	//法線
	D3DXVECTOR3 normal;

	//法線を標準化
	D3DXVec3Normalize(&normal, &plane->normal_);

	//平面と中心点の距離を算出
	float planeToSphereLength = D3DXVec3Dot(&planeToSphereVec, &normal);
	float planeToSphereAbsLength = fabs(planeToSphereLength);

	//進行方向と法線の関係をチェック
	float dot = D3DXVec3Dot(&nowToPlanSphereVec, &normal);

	//平面と平行に移動してめり込んでるスペシャルケース
	if( (IKD_EPSIRON - fabs(dot) > 0.0f) && (planeToSphereAbsLength < sphere->radius_))
	{
		//ずっと抜け出せないので
		time = FLT_MAX;

		//衝突位置は今の位置に指定
		sphere->owner_->SetPosition(sphere->prePos_);

		return true;
	}

	//交差時間の算出
	time = (sphere->radius_ - planeToSphereLength) / dot;

	//衝突位置の算出
	sphere->SetCollisionPos(sphere->prePos_ + time * nowToPlanSphereVec);
	
	//過去の位置の更新
	sphere->SetPrePos(sphere->center_ + sphere->owner_->GetPosition());

	//めりこんでいたら処理を終了
	if(planeToSphereAbsLength < sphere->radius_)
	{
		return true;
	}

	//壁に対して移動が逆向きな衝突していない
	if (dot >= 0)
	{
		return false;
	}

	//時間が0〜1の間にあれば衝突
	if ((0 <= time) && (time <= 1))
	{
		return true;
	}

	return false;

}

bool Collider::IsHitAABBAndSphere(AABBCollider * box, SphereCollider * sphere)
{
	//球の中心位置
	D3DXVECTOR3 spherePos = sphere->center_ + sphere->owner_->GetPosition();

	//AABBの最小位置
	D3DXVECTOR3 boxPosMin = box->boxMin_ + box->owner_->GetPosition();

	//AABBの最大位置
	D3DXVECTOR3 boxPosMax = box->boxMax_ + box->owner_->GetPosition();
	
	//球の中心からAABBへの最短距離べき乗
	float length = 0;

	//AABBの各軸の最短距離を算出
	//球の中心がAABBのよりも左にあった場合
	if (spherePos.x < boxPosMin.x)
	{
		length += (spherePos.x - boxPosMin.x) * (spherePos.x - boxPosMin.x);
	}
	//球の中心がAABBよりも右にあった場合
	if (spherePos.x > boxPosMax.x)
	{
		length += (spherePos.x - boxPosMax.x) * (spherePos.x - boxPosMax.x);
	}
	//球の中心がAABBよりも下にあった場合
	if (spherePos.y < box->boxMin_.y)
	{
		length += (spherePos.y - boxPosMin.y) * (spherePos.y - boxPosMin.y);
	}
	//球の中心がAABBよりも上にあった場合
	if (spherePos.y > boxPosMax.y)
	{
		length += (spherePos.y - boxPosMax.y) * (spherePos.y - boxPosMax.y);
	}
	//球の中心がAABBよりも手前にあった場合
	if (spherePos.z < boxPosMin.z)
	{
		length += (spherePos.z - boxPosMin.z) * (spherePos.z - boxPosMin.z);
	}
	//球の中心がAABBよりも奥にあった場合
	if (spherePos.z > boxPosMax.z)
	{
		length += (spherePos.z - boxPosMax.z) * (spherePos.z - boxPosMax.z);
	}

	//距離が０だったらもうAABBの中に球が入っているので衝突
	//算出した距離（べき乗）を平方根に直してその距離が球の半径よりも短いなら衝突している
	if (length == 0 || (sqrtf(length) <= sphere->radius_))
	{
		return true;
	}

	return false;
}

bool Collider::IsHitOBBAndSphere(OBBCollider * box, SphereCollider * sphere)
{
	//最短距離
	D3DXVECTOR3 vecLength = D3DXVECTOR3(0,0,0);

	//球の中心
	D3DXVECTOR3 spherePos = sphere->center_ + sphere->owner_->GetPosition();

	//oBBの中心
	D3DXVECTOR3 boxPos = box->center_ + box->owner_->GetPosition();

	//各方向の基準となるベクトル（OBB用）
	D3DXVECTOR3 stdVecX = D3DXVECTOR3(1, 0, 0);
	D3DXVECTOR3 stdVecY = D3DXVECTOR3(0, 1, 0);
	D3DXVECTOR3 stdVecZ = D3DXVECTOR3(0, 0, 1);

	//箱の回転に合わせて当たり判定も回転させるため回転行列を生成
	D3DXMATRIX matR, matRX, matRY, matRZ;

	D3DXMatrixRotationX(&matRX, D3DXToRadian(box->owner_->GetRotate().x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(box->owner_->GetRotate().y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(box->owner_->GetRotate().z));

	matR = (matRX * matRY * matRZ);
	
	//XYZの3方向ループ
	for (unsigned int i = 1; i <= 3; i++)
	{
		D3DXVECTOR3 vec;

		//ベクトルを変形
		D3DXVec3TransformCoord(&vec, &box->GetStdVec(i), &matR);

		//変形したベクトルをセット
		box->SetStdVec(vec, i);
		 
		//x方向の最短距離を求める(半分の長さ)
		float length = box->GetBoxHalfSize(i);

		//はみ出し部分を求めるための係数を算出
		float s = D3DXVec3Dot(&(spherePos - boxPos), &box->GetStdVec(i)) / length;

		//マイナス値が含まれるので絶対値に変更
		s = fabs(s);

		//はみ出し部分の算出
		//係数が１をこえていると各辺の外に飛び出しているので計算
		if (s > 1)
		{
			vecLength += (1 - s) * length * box->GetStdVec(i);
		}
	}

	//球の中心からOBBへの最短距離が球の半径より短いなら当たっている
	if (sphere->radius_ > D3DXVec3Length(&vecLength))
	{
		return true;
	}

	return false;
}

void Collider::Draw(D3DXVECTOR3 position,D3DXVECTOR3 rotate)
{
	//移動行列を作成
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, position.x + center_.x, position.y + center_.y, position.z + center_.z);

	D3DXMATRIX matRX, matRY, matRZ;

	//回転行列の作成
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate.z));
	
	//変形行列を渡す
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &(matRX * matRY * matRZ * mat));
	//pMesh_->DrawSubset(0);
}

ColliderShape Collider::GetShape()
{
	return shape_;
}



