#pragma once
#include "Collider.h"

class SphereCollider :public Collider
{
	friend class Collider;

	//球の中心
	D3DXVECTOR3 center_;

	//球の過去の位置（当たり判定のすりぬけ防止用）
	D3DXVECTOR3 prePos_;

	//半径
	float radius_;

	//衝突位置
	D3DXVECTOR3 collisionPos_;

public:
	//引数	:	持ち主、球の中心、球の中心
	SphereCollider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	~SphereCollider();

	bool IsHit(Collider* pTarget) override;
	
	//前のポジションをセットする関数
	//引数	:	前のポジション
	//戻り値	:	なし
	void SetPrePos(D3DXVECTOR3 prepos);

	//衝突位置をセット
	//引数	:	衝突位置
	//戻り値	:	なし
	void SetCollisionPos(D3DXVECTOR3 pos);

	//衝突位置を返す
	//引数	:	なし
	//戻り値	:	衝突位置
	D3DXVECTOR3 GetCollisionPos();
};
