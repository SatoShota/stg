#include "SphereCollider.h"



SphereCollider::SphereCollider(IGameObject* owner, D3DXVECTOR3 center, float radius)
{
	//������̓o�^
	owner_ = owner;

	//���̒��S��o�^
	center_ = center;

	//�O�̋��̈ʒu
	prePos_ = (center_ + owner_->GetPosition());

	//���a��o�^
	radius_ = radius;


	shape_ = SPHERE;

	//�����[�X���͔���g�͕\�����Ȃ�
#ifdef _DEBUG
	//�e�X�g�\���p����g
	D3DXCreateSphere(Direct3D::pDevice, radius, 8, 4, &pMesh_, 0);
#endif
}

SphereCollider::~SphereCollider()
{
}

bool SphereCollider::IsHit(Collider * pTarget)
{
	switch (pTarget->shape_)
	{
	case SPHERE: return IsHitSphereAndSphere(this,(SphereCollider*)pTarget);
	case PLANE:  return IsHitPlaneAndSphere((PlaneCollider*)pTarget, this);
	case AABB:	 return IsHitAABBAndSphere((AABBCollider*)pTarget, this);
	case OBB:    return IsHitOBBAndSphere((OBBCollider*)pTarget, this);
	}

	return false;
}

void SphereCollider::SetPrePos(D3DXVECTOR3 prepos)
{
	prePos_ = prepos;
}

void SphereCollider::SetCollisionPos(D3DXVECTOR3 pos)
{
	collisionPos_ = pos;
}

D3DXVECTOR3 SphereCollider::GetCollisionPos()
{
	return collisionPos_;
}

