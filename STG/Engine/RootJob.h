#pragma once
#include "IGameObject.h"

//ゲームの根本となるクラス
class RootJob :public IGameObject
{
public:
	RootJob();
	~RootJob();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

