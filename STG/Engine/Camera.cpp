#include "Camera.h"
#include"Direct3D.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(D3DXVECTOR3(0,0,0))
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	D3DXMATRIX proj;
	//プロジェクション行列をつくるための関数（１、入れる箱　２　視野角(ズーム)　３、アスペクト比 4,どこから先を表示するか(ニアクリッピング面),５何メートル先まで移すか（ファークリッピング面）
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(70), (float)g.screenWidth / g.screenHeight, 1.0f, 50.0f);
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	//位置と焦点を更新
	Transform();
	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	//ワールド行列を生成して更新
	D3DXMATRIX view;
	
	D3DXMatrixLookAtLH(&view, &worldPosition,&worldTarget, &D3DXVECTOR3(0, 1, 0));
	
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);

}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}

void Camera::SetTarget(const D3DXVECTOR3 target)
{
	target_ = target;
}
