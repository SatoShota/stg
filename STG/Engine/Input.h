#pragma once

#include <d3dx9.h>
#include <dInput.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	//初期化
	//引数:ウィンドウハンドル
	//戻り値:なし
	void Initialize(HWND hWnd);
	
	//メッセージループで入力をひたすらうけとる関数
	//引数:なし
	//戻り値:なし
	void Update();
	
	//キーが押されたかどうか判断する関数
	//引数:判断したいキーの番号
	//戻り値:押されたらtrue,それ以外ならfalse
	bool IsKey(int keyCode); 
	
	//キーが押されたかどうか判断する関数
	//引数:判断したいキーの番号
	//戻り値:押している間true,それ以外ならfalse
	bool IsKeyDown(int keyCode);
	
	//キーが押されたかどうか判断する関数
	//引数:判断したいキーの番号
	//戻り値:キーが押されて離されたらtrue,それ以外ならfalse
	bool IsKeyUp(int keyCode);	
	
	//マウスが押されたかどうか判断する関数
	//引数:判断したいマウスの入力情報
	//戻り値:押されたらtrue,それ以外ならfalse
	bool IsMouseButton(int buttonCode);

	//マウスが押されたかどうか判断する関数
	//引数:判断したいマウスの入力情報
	//戻り値:押されている間true,それ以外ならfalse
	bool IsMouseButtonDown(int buttonCode);

	//マウスが押されたかどうか判断する関数
	//引数:判断したいマウスの入力情報
	//戻り値:離されたらtrue,それ以外ならfalse
	bool IsMouseButtonUp(int buttonCode);

	//マウスカーソルの位置を取得
	//戻値：マウスカーソルの位置
	D3DXVECTOR3 GetMousePosition();

	//そのフレームでのマウスの移動量を取得
	//戻値：X,Y マウスの移動量 ／ Z,ホイールの回転量
	D3DXVECTOR3 GetMouseMove();

	void Release();

};