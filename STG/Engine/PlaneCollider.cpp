#include "PlaneCollider.h"



PlaneCollider::PlaneCollider(IGameObject* owner, D3DXVECTOR3 pos, D3DXVECTOR3 normal)
{
	//持ち主の情報
	owner_ = owner;

	//法線の正規化
	D3DXVec3Normalize(&normal_,&normal);

	//平面のa,b,c,dを設定
	plane_.x = pos.x;
	plane_.y = pos.y;
	plane_.z = pos.z;

	shape_ = PLANE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
		D3DXCreateBox(Direct3D::pDevice,1,1,1, &pMesh_, 0);
#endif
}


PlaneCollider::~PlaneCollider()
{
}

bool PlaneCollider::IsHit(Collider * pTarget)
{
	//あたり判定により処理を変更
	switch (pTarget->shape_)
	{
	case SPHERE: return IsHitPlaneAndSphere(this,(SphereCollider*)pTarget);
	}

	return false;
}

D3DXVECTOR3 PlaneCollider::GetNormal()
{
	return normal_;
}
