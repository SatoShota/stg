#pragma once
#include "Global.h"
#include<fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")

//レイキャスト
struct RayCastData
{
	D3DXVECTOR3 start;		//レイが発射される位置
	D3DXVECTOR3 direction;	//レイが発射される方向
	bool hit;				//レイが当たったか
	float distance;			//レイが当たった点までの距離,length
	D3DXVECTOR3 normal;		//当たったポリゴンの法線
	D3DXVECTOR3 collisionPoint; //衝突点
	
	RayCastData()
	{
		start = D3DXVECTOR3(0, 0, 0);
		direction = D3DXVECTOR3(0, 0, 0);
		hit = false;
		distance = 0.0f;
		normal = D3DXVECTOR3(0, 0, 0);
		collisionPoint = D3DXVECTOR3(0, 0, 0);
	}
};

class Fbx
{
protected:

	//ポリゴン数
	int vertexCount_;	
	
	//頂点数
	int polygonCount_;	
   
	//インデックス数
	int indexCount_;

	//マテリアル数
	int materialCount_;
	
	//マテリアルごとのポリゴン数
	int* polygonCountOfMaterial_;
	
	//頂点情報
	struct Vertex
	{
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線
		D3DXVECTOR2 uv;		//UV
	};

	//FBX使用する際に必要な方々
	FbxManager*		pManager_;
	FbxImporter*	pImporter_;
	FbxScene*		pScene_;
	D3DMATERIAL9*	pMaterial_;

	//頂点情報格納用配列
	Vertex vertexList;

	//頂点情報
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;
	
	LPDIRECT3DTEXTURE9* pTexture_;

	//ノード内を探索
	//引数	:	調べるノード
	//戻り値	:	なし
	void CheckNode(FbxNode* pNode);

	//メッシュ内を探索
	//引数	:　調べるメッシュ
	//戻り値	:  なし
	void CheckMesh(FbxMesh* pMesh);

	LPD3DXEFFECT pEffect_;
	LPDIRECT3DVERTEXDECLARATION9 verDec_;


public:
	Fbx(LPD3DXEFFECT pEffect);
	~Fbx();

	//モデル（FBX）の読み込み
	//引数	:	モデルのファイル名
	//戻り値	:	なし
	void Load(const char* fileName);

	//モデル（FBX）の描画
	//引数	:	モデルの変形行列
	//戻り値	:	なし
	void Draw(const D3DXMATRIX& matrix);

	//レイキャスト
	//引数	:	レイキャストデータ、レイを当てたいモデルの行列
	//戻り値	:	なし
	void RayCast(RayCastData& data, const D3DXMATRIX& matrix);
};
