#include "AABBCollider.h"

AABBCollider::AABBCollider(IGameObject* owner, D3DXVECTOR3 boxMax,D3DXVECTOR3 center)
{
	//持ち主の情報
	owner_ = owner;

	//最大値設定
	boxMax_ = (boxMax_ / 2) + center;

	//最小値の設定（箱の中央を決める為に（最大値 / 2）をする）
	boxMin_ = -(boxMax_ / 2) + center;

	//形の設定
	shape_ = AABB;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, boxMax.x + center.x, boxMax.y + center.y, boxMax.z + center.z, &pMesh_, 0);
#endif
}

AABBCollider::~AABBCollider()
{
}

bool AABBCollider::IsHit(Collider * pTarget)
{
	//衝突した相手により処理を変更
	switch (pTarget->shape_)
	{
	case SPHERE: return IsHitAABBAndSphere(this, (SphereCollider*)pTarget);
	}

	return false;
}

