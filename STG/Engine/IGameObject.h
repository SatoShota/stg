#pragma once
#include<d3dx9.h>
#include<list>
#include<string>
#include "Direct3D.h"
#include "Global.h"

class Collider;

//ゲームオブジェクトクラス
class IGameObject
{
protected:	

	//親の情報
	IGameObject* pParent_;
	
	//コライダー
	Collider* pCollider_;

	//衝突判定リスト
	std::list<Collider*> colliderList_;	

	//子供の情報
	std::list<IGameObject*> childList_;
	
	//オブジェクトの名前
	std::string name_;

	//位置、角度、大きさ
	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	//自分の行列用
	D3DXMATRIX localMatrix_;

	//今いるシーンの行列
	D3DXMATRIX worldMatrix_;

	//時間
	float time_;

	//削除（）のフラグ
	bool dead_;

	//変形
	void Transform();

public:
	
	//コンストラクタ
	IGameObject();
	
	//引数:親のポインタ
	IGameObject(IGameObject* parent);

	//引数 : 親のポインタ、自分の名前
	IGameObject(IGameObject* parent, const std::string& name);
	
	//デストラクタ
	virtual ~IGameObject();

	//クラスを作る際のテンプレ
	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//親の情報を取得
		T* p = new T(parent);
		//親の子供情報にこれをいれる
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}

	//初期化
	virtual void Initialize() = 0;

	//更新、子クラスの更新用
	virtual void Update() = 0;
	void UpdateSub();

	//描画、子クラスの描画用
	virtual void Draw() = 0;
	void DrawSub();

	//開放、子クラスの解放用
	virtual void Release() = 0;
	void ReleaseSub();
	
	//衝突検知
	//引数	:	調べる相手
	//戻り値	:	なし
	void Collision(IGameObject* targetOblect);
	
	//衝突処理
	//引数	:	当たった相手,自分の衝突検知したコライダー,相手の衝突検知したコライダー
	//戻り値	:	なし
	virtual void OnCollision(IGameObject* pTarget, Collider* pMyCollider, Collider* pCollider) {};

	//テスト用の衝突判定枠を表示
	//引数	:	なし
	//戻り値	:	なし
	void CollisionDraw();

	//削除のフラグを立てる
	//引数	:	なし
	//戻り値	:	なし
	void KillMe();

	//子供の捜索
	//引数　	:	オブジェクトの名前
	//戻り値	:	指定したオブジェクトのアドレス
	IGameObject* FindChildObject(const std::string& name);

	//こどもの追加
	//引数	:　	追加する情報
	//戻り値	:	なし
	void PushBackChild(IGameObject* obj);
		
	//位置情報を取得
	//引数	:	なし
	//戻り値	:	位置
	D3DXVECTOR3 GetPosition();

	//回転情報を取得
	//引数	:	なし
	//戻り値	:	回転
	D3DXVECTOR3 GetRotate();

	//拡大縮小情報を取得
	//引数	:	なし
	//戻り値	:	拡大率
	D3DXVECTOR3 GetScale();

	//すべてを含めた変形情報取得
	//引数	:	なし
	//戻り値	:	変形行列
	D3DXMATRIX GetWorldMatrix();

	//自分だけの変形情報
	//引数	:	なし
	//戻り値	:	変形行列
	D3DXMATRIX GetLocalMatrix();

	//親の情報を取得
	//引数	:	なし
	//戻り値	:	親の情報	
	IGameObject* GetParent();

	//コライダー情報を取得
	//引数	:	なし
	//戻り値	:	コライダー情報
	Collider* GetCollider();

	//子供の情報を追加
	//引数	:	なし
	//戻り値	:	こどもリストの先頭
	std::list<IGameObject*>* GetChildList();

	//オブジェクトの名前を取得
	//引数	:	なし
	//戻り値	:	自分の名前
	const std::string& GetObjectName(void) const;

	//位置を設定
	//引数	:	位置
	//戻り値	:	なし
	void SetPosition(const D3DXVECTOR3 position);

	//回転情報を設定
	//引数	:	回転
	//戻り値	:	なし
	void SetRotate(const D3DXVECTOR3 rotate);

	//拡大率の設定
	//引数	:	拡大率
	//戻り値	:	なし
	void SetScale(const D3DXVECTOR3 scale);

	//あたり判定をセット
	//引数	:	あたり判定
	//戻り値	:	なし
	void SetCollider(Collider* collider);

};

