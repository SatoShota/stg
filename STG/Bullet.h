#pragma once
#include "Engine/Global.h"

const D3DXVECTOR3 GRAVITY_ACCELERATION = D3DXVECTOR3(0,M_S(-9.8f), 0);		//重力加速度
const float REFLECTION＿COEFFICIENT = 1.0f;									//反発係数
const float BULLET_RADIUS = 0.1f;

//弾を管理するクラス
class Bullet : public IGameObject
{
	//モデル番号
	int hModel_;
	
	//飛ぶ方向
	D3DXVECTOR3 moveDir_;

	//力の合計
	D3DXVECTOR3 totalForce_;

	//力
	D3DXVECTOR3 force_;

	//加速度
	D3DXVECTOR3 acceleration_;

	//速度
	D3DXVECTOR3 velocity_;
		
	//質量
	float mass_;

public:

	D3DXVECTOR3 colPos;

	//コンストラクタ
	Bullet(IGameObject* parent);

	//デストラクタ
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突処理
	void OnCollision(IGameObject* pTarget, Collider* pMyCollider, Collider* pCollider) override;

	//弾の飛ぶ方向をセットするために使用
	//引数	:	飛ぶ方向
	//戻り値	:	なし
	void SetMoveDir(D3DXVECTOR3 moveDir);

};
