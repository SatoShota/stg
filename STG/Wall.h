#pragma once
#include "Engine/IGameObject.h"

//壁を管理するクラス
class Wall : public IGameObject
{
	int hModel_;

	//シェーダー用
	LPD3DXEFFECT	pEffect_;
	LPDIRECT3DTEXTURE9 pToonTex_;


public:
	//コンストラクタ
	Wall(IGameObject* parent);

	//デストラクタ
	~Wall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};