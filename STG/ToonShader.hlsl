//グローバル変数（アプリ側から渡される情報）
float4x4 WVP;				//ワールド、ビュー、プロジェクション行列を合成したもの
float4x4 SR;				//回転行列と拡大の逆行列
float4x4 W;					//ワールド行列
float4	 LIGHT_DIR;			//光の方向	
float4	 DIFFUSE_COLOR;		//拡散反射光
float4   AMBIENT_COLOR;		//環境光
float	 SPECULER_POWER;	//鏡面反射
float4	 SPECULER_COLOR;	//鏡面反射光
float4	 CAMERA_POS;		//カメラ位置
texture	 TEXTURE;			//テクスチャ情報
bool	 TEX;
texture	 TEXTURE_TOON;


//サンプラー
//テクスチャーを張るのに必要
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;

	//アンチエイリアシング
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

	//貼り方の指定テクスチャの並べ方
	AddressU = Wrap;
	AddressV = wrap;
};

sampler toonSampler = sampler_state
{
	Texture = <TEXTURE_TOON>;
	AddressU = Clamp;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};

//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float4 normal : NORMAL;			//法線
	float4 eye	  : TEXCOORD1;		//視線
	float2 uv	  : TEXCOORD0;		//UV座標
};

//【頂点シェーダー】
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;

	//位置を設定
	outData.pos = mul(pos, WVP);

	//法線の変形と正規化
	normal = mul(normal, SR);
	normal = normalize(normal);
	outData.normal = normal;

	//位置にワールド行列をかけて変形
	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	//UVを設定
	outData.uv = uv;

	//まとめて出力
	return outData;
}

//【ピクセルシェーダー】
float4 PS(VS_OUT inData) : COLOR
{
	//法線を正規化
	inData.normal = normalize(inData.normal);

	//光の向きを設定して正規化
	float4 lightDir = LIGHT_DIR;
	lightDir = normalize(lightDir);	

	//拡散反射光を設定
	float u = dot(inData.normal, -lightDir);
	float4 diffuse = tex2D(toonSampler, float2(u, 0));
	diffuse.a = 1;

	//モデルにテクスチャが貼ってあるかを調べて色を決定
	if (!TEX)
	{
		diffuse *= tex2D(toonSampler, inData.uv);
	}
	else
	{
		diffuse *= DIFFUSE_COLOR;
	}

	//環境光
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射
	float4 R = reflect(lightDir, inData.normal);
	float4 speculer = pow(dot(R, inData.eye), SPECULER_POWER) * 2 * SPECULER_COLOR;

	//頂点シェーダーで色を求めてるので、そのまま出力
	return diffuse;
}

float4 VS_TOON(float4 pos : POSITION, float4 normal : NORMAL) : SV_POSITION
{
	//法線を設定
	normal.w = 0;

	//枠の位置を設定
	pos += normal / 35;

	pos = mul(pos, WVP);

	return pos;
}

float4 PS_TOON(float4 pos : SV_POSITION) : COLOR
{
	return float4(0,0,0,1);
}

//【テクニック】
//トゥーン用の枠と通常表示の両方を用意
technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}

	pass
	{
		VertexShader = compile vs_3_0 VS_TOON();
		PixelShader = compile ps_3_0 PS_TOON();
	}
}