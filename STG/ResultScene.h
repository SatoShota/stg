#pragma once
#include "Engine/global.h"
#include "Score.h"

class Score;

const float TIME_COUNT = 360.0;

typedef	enum
{		
	SCORE_SHOW,				//スコア表示
	SCORE_AUTO_SHOW,		//自動スコア表示
	SCORE_SOUND,			//スコア表示音
	HIGHSCORE_SHOW,			//ハイスコア表示
	HIGH_SCORE_AUTO_SHOW,	//自動スコア表示
	HIGHSCORE_SOUND,		//ハイスコア表示音
	HIGHSCORE_UPADATE,		//ハイスコア更新
	CHANGE_SCENE,			//シーン切り替え
	AUTO_STOP				//自動でスコアを表示
}RESULT_STATE;


//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
	//スコア
	Score* pScore_;

	//時間の計測をするかどうか
	bool countStop_;

	//リザルト表示状況
	RESULT_STATE scoreState_;
	RESULT_STATE autoScoreState_;

	//時間計測
	float time_;

public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
  ResultScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;

  //スコアを表示
  void ScoreDraw(RESULT_STATE &scoreState);
};