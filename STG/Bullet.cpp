#include "Bullet.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/PlaneCollider.h"
#include "Rifle.h"
#include "Toy.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"),hModel_(-1),moveDir_(D3DXVECTOR3(0, 0, 0)),force_(D3DXVECTOR3(0,0,0)), acceleration_(D3DXVECTOR3(0, 0, 0))
	, velocity_(D3DXVECTOR3(0, 0, 0)),totalForce_(D3DXVECTOR3(0, 0, 0))
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルの読み込み
	hModel_ = Model::Load("Data/model/Play_bullet.fbx");
	assert(hModel_ != -1);

	//あたり判定を作成
	SphereCollider * collider = new SphereCollider(this,position_, BULLET_RADIUS);
	SetCollider(collider);

	//撃った銃のポインタを取得
	IGameObject* pRifle = pParent_->FindChildObject("Rifle");

	//銃口の向きをもらって飛ぶ方向を決定
	moveDir_ = ((Rifle*)pRifle)->GetBulletMove_();
	
	//初速度の設定
	velocity_ = moveDir_ * 10;
}

//更新
void Bullet::Update()
{	
	//速度の計算
	velocity_ +=  GRAVITY_ACCELERATION;

	//位置の更新
	position_ += M_S(velocity_);
}

//描画
void Bullet::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

void Bullet::OnCollision(IGameObject * pTarget, Collider* pMyCollider, Collider* pCollider)
{
	//衝突した相手がおもちゃ
	if (pTarget->GetObjectName() == "Toy")
	{
		//toyクラスのポインタ生成
		Toy* pToy = (Toy*)pTarget;

		//レイキャストのデータを生成
		RayCastData ray;
		ray.start = position_ + (position_ - pTarget->GetPosition());	//レイの発射位置をセット
		ray.direction = pTarget->GetPosition() - position_;				//飛ばす方向をセット（弾からおもちゃへの中心ベクトル）
		Model::RayCast(pToy->GetModelHandle(),ray);						//レイを発射

		//反射ベクトル計算
		D3DXVECTOR3 normal_n;
		D3DXVECTOR3 reflection;
		D3DXVec3Normalize(&normal_n, &ray.normal);
		D3DXVec3Normalize(&reflection, &(velocity_ - 2.0f * D3DXVec3Dot(&velocity_, &normal_n) * normal_n));

		//反射
		velocity_ = reflection * D3DXVec3Length(&velocity_);

		colPos = ray.collisionPoint;
	}
	//衝突したのが地面
	else if (pTarget->GetObjectName() == "Ground")
	{
		//平面の情報
		PlaneCollider* pPlane;
		pPlane = (PlaneCollider*)pCollider;

		//球の情報
		SphereCollider* pSphere;
		pSphere = (SphereCollider*)pMyCollider;

		//反射ベクトル計算
		D3DXVECTOR3 normal_n;
		D3DXVECTOR3 reflection;
		D3DXVec3Normalize(&normal_n, &pPlane->GetNormal());
		D3DXVec3Normalize(&reflection, &(velocity_ - 2.0f * D3DXVec3Dot(&velocity_, &normal_n) * normal_n));

		//めりこんでいるので衝突位置に変更
		position_ = pSphere->GetCollisionPos();

		//反射
		velocity_ = reflection * D3DXVec3Length(&velocity_);
	}


}

void Bullet::SetMoveDir(D3DXVECTOR3 moveDir)
 {
	D3DXVec3Normalize(&moveDir, &moveDir);
	moveDir_ = moveDir;
}


	

