#pragma once
#include "Engine/IGameObject.h"
#include "Wall.h"
#include "Ground.h"

const D3DXVECTOR3 RACK_POS		=	D3DXVECTOR3(0, 1, 15);	//棚の初期位置
const D3DXVECTOR3 TABLE_POS		=	D3DXVECTOR3(0, 0, 10);	//テーブルの初期位置
const D3DXVECTOR3 GROUND_POS	=	D3DXVECTOR3(0, 0, 0);	//地面の初期位置

//ステージを管理するクラス
class Stage : public IGameObject
{
	//地面
	Ground* pGround_;

	//ステージの壁
	Wall* pWall_;

	//空
	int hModel_;

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};