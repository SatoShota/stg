#pragma once
#include "PlayScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject *parent)
	: IGameObject(parent, "PlayScene"), changeScene_(false)
{
}

//初期化
void PlayScene::Initialize()
{
	//プレイヤーを生成
	pPlayer_ = CreateGameObject<Player>(this);
	pPlayer_->SetPosition(PLAYER_POS);

	//スコアの生成
	pScore_ = CreateGameObject<Score>(this);
	
	//ステージ
	pStage_ = CreateGameObject<Stage>(this);

}

//更新
void PlayScene::Update()
{
	//シーンを切り替える
	if (changeScene_)
	{
		time_ += 0.2f;

		if (time_ > 30.0f)
		{
			SceneManager::ChangeScene(SCENE_ID_RESULT);
		}
	}

}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
	
}

void PlayScene::ChangeScene()
{
	//スコアを記録
	pScore_->SaveScoreFile("Data/Score.txt");

	//シーンを移動
	changeScene_ = true;
}
