#pragma once

#include "Engine/global.h"
#include "Engine/Image.h"

enum IMAGE_NAME
{
	BACK,		//背景
	LOGO,		//タイトルロゴ
	GUIDE,		//キーの操作
	IMAGE_HANDLE_MAX	
};

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	//画像のハンドル
	int hImage_[IMAGE_HANDLE_MAX];

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};