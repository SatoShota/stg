#include "ResultScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Audio.h"
#include "Score.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
    : IGameObject(parent, "ResultScene"),countStop_(true),time_(0.0f)
{

}

//初期化
void ResultScene::Initialize()
{
	pScore_ = CreateGameObject<Score>(this);

	//BGMの再生
	Audio::Play("Score_BGM");

	//スコアを表示
	scoreState_ = SCORE_SHOW;
	autoScoreState_ = SCORE_AUTO_SHOW;

	//時間設定
	time_ = TIME_COUNT;
}

//更新
void ResultScene::Update()
{

	//スペースキーを押したらシーン移動
	if (Input::IsKeyUp(DIK_SPACE))
	{
		//スコアを表示
		ScoreDraw(scoreState_);
	}

}

//描画
void ResultScene::Draw()
{	
}

//開放
void ResultScene::Release()
{
}

void ResultScene::ScoreDraw(RESULT_STATE& scoreState)
{
	//状態で管理
	switch (scoreState)
	{
		//スコアを表示する状態
	case SCORE_SHOW:
		//ドラムロールを停止
		Audio::Stop("Score_BGM");

		//効果音を鳴らす
		Audio::Play("Score_Show");

		//スコアを表示
		((Score*)pScore_)->SetScoreShowFlag(true);

		//状態をハイスコア表示に変更
		scoreState = HIGHSCORE_SHOW;

		break;

		//ハイスコアを表示する状態
	case HIGHSCORE_SHOW:

		//効果音を鳴らす
		Audio::Play("Highscore_Show");

		//ハイスコアを表示
		((Score*)pScore_)->SetHighScoreShowFlag(true);

		//ハイスコアを更新していたなら
		if (pScore_->GetHighScoreUpdateFlag())
		{
			//ハイスコア更新状態に変更
			scoreState = HIGHSCORE_UPADATE;
		}
		else
		{
			//シーン切り替え状態に移動
			scoreState = CHANGE_SCENE;
		}

		break;

		//ハイスコアを表示する状態
	case HIGHSCORE_UPADATE:

		//効果音を鳴らす
		Audio::Play("Highscore_Update");

		//ハイスコア更新を表示
		pScore_->SetHighScoreUpdateShowFlag(true);

		//シーン切り替え状態変更
		scoreState = CHANGE_SCENE;

		break;

		//シーンを切り替える状態
	case CHANGE_SCENE:

		//シーンを切り替える
		SceneManager::ChangeScene(SCENE_ID_TITLE);

		break;

	}
}