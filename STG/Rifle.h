#pragma once
#include "Engine/Global.h"
#include "Bullet.h"

const unsigned int BULLET_COUNT = 10;	//初期の所持弾数
const D3DXVECTOR3 MUZZLE_DIR = D3DXVECTOR3(0.0f, 0.15f, 1.0f);		//銃口の位置

class Bullet;

//銃を管理するクラス
class Rifle : public IGameObject
{
	//画像の識別番号
	int hImage_;

	//残段数
	unsigned int bulletCount_;

	//弾を管理するためのポインタ
	Bullet* pBullet_;

	//銃口の向き
	D3DXVECTOR3 muzzleDir_;

	//弾が飛ぶ方向
	D3DXVECTOR3 bulletMove_;

public:
	//コンストラクタ
	Rifle(IGameObject* parent);

	//デストラクタ
	~Rifle();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void Shot();

	void CreateBulletMove();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//弾の起動を取得する
	//引数	:	なし
	//戻り値	:	弾の軌道
	D3DXVECTOR3 GetBulletMove_();
};