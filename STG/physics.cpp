#include "physics.h"

D3DXVECTOR3 physics::CalcReflection(D3DXVECTOR3 moveDir, D3DXVECTOR3 normal)
{
	D3DXVECTOR3 reflect;
	D3DXVec3Normalize(&reflect, &(moveDir - 2.0f * D3DXVec3Dot(&moveDir, &normal) * normal));

	return reflect;
}
