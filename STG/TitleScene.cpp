#include "TitleScene.h"
#include <string>
#include "Engine/SceneManager.h"
#include "Engine/Audio.h"


//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene")
{
	//初期化
	for (int i = 0; i < IMAGE_HANDLE_MAX; i++)
	{
		hImage_[i] = -1;
	}
	
}

//初期化
void TitleScene::Initialize()
{
	//画像のパスをあらかじめ格納
	std::string fileName[IMAGE_HANDLE_MAX] =
	{
		"Data/Image/BackGroundTitleImage.png",
		"Data/Image/Title_Logo.png",
		"Data/Image/Title_KeyGuide.png"
	};

	//画像読み込み
	for (int i = 0; i < IMAGE_HANDLE_MAX; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
		assert(hImage_[i] != -1);
	}

	//BGMを再生
	Audio::Play("Title_BGM");
}

//更新
void TitleScene::Update()
{
	//スペースキーを押したらシーン移動
	if(Input::IsKeyUp(DIK_SPACE))
	{
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	//画像を表示
	for (int i = 0; i < IMAGE_HANDLE_MAX; i++)
	{
		Image::SetMatrix(hImage_[i], worldMatrix_);
		Image::Draw(hImage_[i]);
	}
}

//開放
void TitleScene::Release()
{
	//BGMを停止
	Audio::Stop("Title_BGM");
}