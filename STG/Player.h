#pragma once
#include "Engine/Global.h"
#include "Rifle.h"

const float MOVE_SPEED = 0.1f;		//移動速度
const float CAMERA_SPEED = 0.4f;	//視点速度
const float MOVE_LIMIT_FRONT = 7.0f;
const float MOVE_LIMIT_BACK = 1.0f;
const float MOVE_LIMIT_LEFT = 5.0f;
const float MOVE_LIMIT_RIGHT = -5.0f;
const float ROTATE_LIMIT = -90.0f;


enum
{
	BUTTON,
	BUTTON_A,
	BUTTON_W,
	BUTTON_S,
	BUTTON_D,
	BUTTON_SPACE,
	BUTTON_UP,
	BUTTON_LEFT,
	BUTTON_RIGHT,
	BUTTON_DOWN,
	BUTTON_MAX
};

class Camera;
class Rifle;

//自機を管理するクラス
class Player : public IGameObject
{
	//カメラを管理するためのポインタ
	Camera* pCamera_;

	//銃を管理するためのポインタ
	Rifle* pRifle_;

	//画像のハンドル
	int hImage_[BUTTON_MAX];

	//移動の処理をまとめた関数
	//プレイヤー以外が使うことはないのでprivateおいておく
	void Move();

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};