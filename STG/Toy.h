#pragma once
#include "Engine/Global.h"

const D3DXVECTOR3 TOY_COLLIDER_SIZE = D3DXVECTOR3(1, 1, 1);	//コライダーの大きさ
const int TOY_COUNT_MAX = 8;		//おもちゃ配置の最大数

//景品（おもちゃ）を管理するクラス
class Toy : public IGameObject
{
	//モデルの識別番号
	int hModel_;

	//シェーダー用
	LPD3DXEFFECT	pEffect_;

	LPDIRECT3DTEXTURE9 pToonTex_;



public:
	//コンストラクタ
	Toy(IGameObject* parent);

	//デストラクタ
	~Toy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突処理
	void OnCollision(IGameObject* pTarget, Collider* pMyCollider, Collider* pCollider) override;

	//レイキャストに使用するモデルの番号を返す
	//引数	:	なし
	//戻り値	:	モデル番号
	int GetModelHandle();
};

