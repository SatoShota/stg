#pragma once
#include"Engine/Global.h"
#include "Bullet.h"
#include "Player.h"
#include "Score.h"
#include "Stage.h"

const D3DXVECTOR3 PLAYER_POS = D3DXVECTOR3(0, 2.5, 3);	//プレイヤーの初期位置


//前方宣言
class Score;
class Stage;
class Player;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//プレイヤー
	Player* pPlayer_;

	//ステージ
	Stage* pStage_;

	//スコア
	Score* pScore_;

	//シーンを切り替えるかどうか
	bool changeScene_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//リザルトシーンに移行する
	//引数	:	なし
	//戻り値	:	なし
	void ChangeScene();
};