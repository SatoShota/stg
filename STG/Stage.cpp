#include "Stage.h"
#include "Engine/Model.h"
#include "player.h"
#include  "Toy.h"

Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//地面の生成
	pGround_ = CreateGameObject<Ground>(this);
	pGround_->SetPosition(GROUND_POS);

	pWall_ = CreateGameObject<Wall>(this);
	pWall_->SetPosition(D3DXVECTOR3(10, 7.5, 20));
	pWall_->SetRotate(D3DXVECTOR3(0, 0, 90));

	//棚に的を配置
	for (int i = -2; i <= 2; i += 2)
	{
		CreateGameObject<Toy>(pParent_)->SetPosition(RACK_POS + D3DXVECTOR3((float)i, 1.50f, 0.0f));
	}
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
}

//開放
void Stage::Release()
{
}