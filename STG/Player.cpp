#include "Player.h"
#include "Engine/Camera.h"
#include "Engine/Image.h"
//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player")
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//カメラの設定
	pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetTarget(D3DXVECTOR3(0, 0, 1));

	//銃の設定
	pRifle_ = CreateGameObject<Rifle>(this);

	////画像のパスをあらかじめ格納
	std::string fileName[BUTTON_MAX] =
	{
		"Data/Image/Button_off.png",
		"Data/Image/Button_A.png",
		"Data/Image/Button_W.png",
		"Data/Image/Button_S.png",
		"Data/Image/Button_D.png",
		"Data/Image/Button_Space.png",
		"Data/Image/Button_Up.png",
		"Data/Image/Button_Left.png",
		"Data/Image/Button_Right.png",
		"Data/Image/Button_Down.png"
	};

	//////画像読み込み
	for (int i = 0; i < BUTTON_MAX; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}
	
}

//更新
void Player::Update()
{

}

void Player::Move()
{
	//キャラ移動
	//Aをおしたら左に移動
	if (Input::IsKey(DIK_A) && position_.x > MOVE_LIMIT_RIGHT)
	{
		position_.x -= MOVE_SPEED;	
		
		Image::SetMatrix(hImage_[BUTTON_A], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_A]);
	}
	//Dをおしたら右に移動
	if (Input::IsKey(DIK_D) && position_.x < MOVE_LIMIT_LEFT)
	{
		position_.x += MOVE_SPEED;

		Image::SetMatrix(hImage_[BUTTON_D], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_D]);
	}
	if (Input::IsKey(DIK_W) && position_.z < MOVE_LIMIT_FRONT)
	{
		position_.z += MOVE_SPEED;

		Image::SetMatrix(hImage_[BUTTON_W], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_W]);
	}
	//Dをおしたら右に移動
	if (Input::IsKey(DIK_S) && position_.z > MOVE_LIMIT_BACK)
	{
		position_.z -= MOVE_SPEED;

		Image::SetMatrix(hImage_[BUTTON_S], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_S]);
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		pRifle_->Shot();

		Image::SetMatrix(hImage_[BUTTON_SPACE], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_SPACE]);
	}

	//視点
	//←をおしたら左視点移動
	if (Input::IsKey(DIK_LEFT) && rotate_.y > ROTATE_LIMIT)
	{
		rotate_.y -= CAMERA_SPEED;

		Image::SetMatrix(hImage_[BUTTON_LEFT], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_LEFT]);
	}
	
	//→をおしたら右視点移動
	if (Input::IsKey(DIK_RIGHT) && rotate_.y < -ROTATE_LIMIT)
	{
		rotate_.y += CAMERA_SPEED;

		Image::SetMatrix(hImage_[BUTTON_RIGHT], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_RIGHT]);
	}
	//↑をおしたら上視点移動
	if (Input::IsKey(DIK_UP) && rotate_.x > ROTATE_LIMIT)
	{
		rotate_.x -= CAMERA_SPEED;

		Image::SetMatrix(hImage_[BUTTON_UP], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_UP]);
	}
	//↓をおしたら下視点移動
	if (Input::IsKey(DIK_DOWN) && rotate_.x < -ROTATE_LIMIT)
	{
		rotate_.x += CAMERA_SPEED;

		Image::SetMatrix(hImage_[BUTTON_DOWN], pParent_->GetWorldMatrix());
		Image::Draw(hImage_[BUTTON_DOWN]);
	}

}

//描画
void Player::Draw()
{
	Image::SetMatrix(hImage_[BUTTON], pParent_->GetWorldMatrix());
	Image::Draw(hImage_[BUTTON]);

	Move();
}

//開放
void Player::Release()
{
}