#include "Wall.h"
#include "Engine/Model.h"
#include "Player.h"
//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall")
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	//シェーダー
	//HLSLファイルからシェーダーを作成
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice,
		"ToonShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
		&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	//アニメ風シェード用画像
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, "data/toon.png",
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, NULL, NULL, NULL, &pToonTex_);

	hModel_ = Model::Load("Data/Model/Wall.fbx",pEffect_);
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);

	//シェーダー

	//ビュー行列を取得
	D3DXMATRIX view;
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列取得
	D3DXMATRIX proj;
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ワールド、ビュー、プロジェクション行列を合成
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	//D3DXMatrixInverse(&scale, nullptr, &scale);

	//行列を合成
	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;

	//ライトの向きをシェーダに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice->GetLight(0, &lightState);

	//カメラの位置を取得
	IGameObject* ps = GetParent()->GetParent();
	Player* p = (Player*)ps->FindChildObject("Player");

	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&p->GetPosition());

	//作成した情報をシェーダーに渡す
	pEffect_->SetMatrix("WVP", &matWVP);
	pEffect_->SetMatrix("RS", &mat);
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);
	pEffect_->SetMatrix("W", &worldMatrix_);
	pEffect_->SetTexture("TEXTURE_TOON", pToonTex_);

	//描画開始
	pEffect_->Begin(NULL, 0);

	//トゥーンの輪郭を表示
	pEffect_->BeginPass(1);

	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);

	Model::Draw(hModel_);

	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	pEffect_->EndPass();

	//通常表示
	pEffect_->BeginPass(0);

	Model::Draw(hModel_);

	pEffect_->EndPass();

	pEffect_->End();
}

//開放
void Wall::Release()
{
}