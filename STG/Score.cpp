#include "Score.h"
#include "Engine/Image.h"

unsigned int Score::score_ = 0;
unsigned int Score::highScore_ = 0;

//コンストラクタ
Score::Score(IGameObject * parent)
	:IGameObject(parent, "Score"),isHighScoreUpdate_(false), scoreShowFlag_(false),
	highScoreShowFlag_(false), highScoreUpdateShowFlag_(false),scoreShowSoundFlag_(true),highScoreShowSoundFlag_(true)
{
	//初期化
	for (int i = 0; i < SCORE_IMAGE_MAX; i++)
	{
		hImage_[i] = -1;
	}

	for (int i = 0; i < SCORE_NUMBER_MAX; i++)
	{
		hNumImage_[i] = -1;
	}
}

//デストラクタ
Score::~Score()
{
}

//初期化
void Score::Initialize()
{
	//画像のパスをあらかじめ格納
	std::string fileName[SCORE_IMAGE_MAX] =
	{
		"Data/Image/BackGroundImage.png",
		"Data/Image/Result_Scoreboard.png",
		"Data/Image/Result_HighScoreBoard.png",
		"Data/Image/Result_KeyGuide.png",
		"Data/Image/Result_HighScoreUpadate.png"
	};

	//画像読み込み
	for (int i = 0; i < SCORE_IMAGE_MAX; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
		assert(hImage_[i] != -1);
	}

	//画像のパスをあらかじめ格納
	std::string fileName2[SCORE_NUMBER_MAX] =
	{
		"Data/Image/ScoreNumber_0.png",
		"Data/Image/ScoreNumber_1.png",
		"Data/Image/ScoreNumber_2.png",
		"Data/Image/ScoreNumber_3.png",
		"Data/Image/ScoreNumber_4.png",
		"Data/Image/ScoreNumber_5.png",
		"Data/Image/ScoreNumber_6.png",
		"Data/Image/ScoreNumber_7.png",
		"Data/Image/ScoreNumber_8.png",
		"Data/Image/ScoreNumber_9.png"
	};

	for (int i = 0; i < SCORE_NUMBER_MAX -1; i++)
	{
		hNumImage_[i] = Image::Load(fileName2[i]);
		assert(hNumImage_[i] != -1);
			
	}

	if (pParent_->GetObjectName() == "ResultScene")
	{
		ReadScoreFile("Data/Score.txt");
		ReadHighScoreFile("Data/highScore.txt");
	}
	
	//ハイスコアよりも今のスコアが高いなら更新
	if (highScore_ < score_)
	{
		highScore_ = score_;
		isHighScoreUpdate_ = true;
		SaveHighScoreFile("Data/highScore.txt");
	}
			
}

//更新
void Score::Update()
{
}

//描画
void Score::Draw()
{
	if (pParent_->GetObjectName() == "ResultScene")
	{
		//画像を表示
		for (int i = 0; i < SCORE_IMAGE_MAX - 1; i++)
		{
			Image::SetMatrix(hImage_[i], worldMatrix_);
			Image::Draw(hImage_[i]);
		}
		
		if (highScoreUpdateShowFlag_)
		{
			Image::SetMatrix(hImage_[SCOREUPADATE], worldMatrix_);
			Image::Draw(hImage_[SCOREUPADATE]);
		}
		

		//スコアを表示
		ShowScore();
		ShowHighScore();
	}

}

//開放
void Score::Release()
{
}

void Score::AddScore(unsigned int score)
{
	score_ += score;
}

void Score::ReadScoreFile(std::string fileName)
{
	//ファイルを開く
	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	//ファイルのサイズ（文字数）を調べる
	DWORD fileSize = GetFileSize(hFile, NULL);

	//すべての文字を入れられる配列を用意
	char* data;
	data = new char[fileSize];

	//ファイルの中身を配列に読み込む
	DWORD dwBytes = 0;
	ReadFile(hFile, data, fileSize, &dwBytes, NULL);

	//開いたファイルを閉じる
	CloseHandle(hFile);

	score_ = atoi(data);

	delete data;
}

void Score::SaveScoreFile(std::string fileName)
{
	//ファイルを開く
	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	
	//書き込み位置
	DWORD dwBytes = 0; 

	//スコアを数値から文字列に変換
	char cstr[10];
	sprintf_s(cstr, "%d", score_);

	//ファイルに書き込み
	WriteFile(hFile, cstr, (DWORD)strlen(cstr), &dwBytes, NULL);

	//ファイルを閉じる
	CloseHandle(hFile);
}

void Score::ReadHighScoreFile(std::string fileName)
{
	//ファイルを開く
	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	//ファイルのサイズ（文字数）を調べる
	DWORD fileSize = GetFileSize(hFile, NULL);

	//すべての文字を入れられる配列を用意
	char* data;
	data = new char[fileSize];

	//ファイルの中身を配列に読み込む
	DWORD dwBytes = 0;
	ReadFile(hFile, data, fileSize, &dwBytes, NULL);

	//開いたファイルを閉じる
	CloseHandle(hFile);

	highScore_ = atoi(data);

	delete data;
}

void Score::SaveHighScoreFile(std::string fileName)
{
	//ファイルを開く
	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	//書き込み位置
	DWORD dwBytes = 0;

	//スコアを数値から文字列に変換
	char cstr[10];
	sprintf_s(cstr, "%d", highScore_);

	//ファイルに書き込み
	WriteFile(hFile, cstr, (DWORD)strlen(cstr), &dwBytes, NULL);

	//ファイルを閉じる
	CloseHandle(hFile);
	
}

void Score::ShowScore()
{
	if (scoreShowFlag_)
	{
		//スコアを数値から文字列に変換
		char cstr[10];
		sprintf_s(cstr, "%d", score_);

		//移動行列の作成
		D3DXMATRIX matT;

		//スコアを表示
		for (unsigned int i = 0; i < strlen(cstr); i++)
		{
			//一桁目を(600,60,0)の位置に表示しその後xを-100ずらした位置に表示
			D3DXMatrixTranslation(&matT, 600.0f - (i * 100.0f), 60.0f, 0.0f);

			//一文字の数字はatoiでは変換できないので0-9の文字コードから'0'を取り除く
			Image::SetMatrix(hNumImage_[cstr[(strlen(cstr) - 1) - i] - '0'], matT * worldMatrix_);
			Image::Draw(hNumImage_[cstr[(strlen(cstr) - 1) - i] - '0']);
		}
	}
}

void Score::ShowHighScore()
{
	if (highScoreShowFlag_)
	{
		//スコアを数値から文字列に変換
		char cstr[10];
		sprintf_s(cstr, "%d", highScore_);

		//移動行列の作成
		D3DXMATRIX matT;

		//スコアを表示
		for (unsigned int i = 0; i < strlen(cstr); i++)
		{
			//一桁目を(600,60,0)の位置に表示しその後xを-100ずらした位置に表示
			D3DXMatrixTranslation(&matT, 600.0f - (i * 100.0f), 280.0f, 0.0f);

			//一文字の数字はatoiでは変換できないので0-9の文字コードから'0'を取り除く
			Image::SetMatrix(hNumImage_[cstr[(strlen(cstr) - 1) - i] - '0'], (matT * worldMatrix_));
			Image::Draw(hNumImage_[cstr[(strlen(cstr) - 1) - i] - '0']);
		}
	}
}

void Score::SetScoreShowFlag(bool flag)
{
	scoreShowFlag_ = flag;
}

void Score::SetScoreShowSoundFlag(bool flag)
{
	scoreShowSoundFlag_ = flag;
}

void Score::SetHighScoreShowFlag(bool flag)
{
	highScoreShowFlag_ = flag;
}

void Score::SetHighScoreShowSoundFlag(bool flag)
{
	highScoreShowSoundFlag_ = flag;
}

void Score::SetHighScoreUpdateShowFlag(bool flag)
{
	highScoreUpdateShowFlag_ = flag;
}

bool Score::GetScoreShowFlag()
{
	return scoreShowFlag_;
}

bool Score::GetScoreShowSoundFlag()
{
	return scoreShowSoundFlag_;
}

bool Score::GetHighScoreShowFlag()
{
	return highScoreShowFlag_;
}

bool Score::GetHighScoreShowSoundFlag()
{
	return scoreShowSoundFlag_;
}

bool Score::GetHighScoreUpdateFlag()
{
	return isHighScoreUpdate_;
}
