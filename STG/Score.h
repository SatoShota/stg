#pragma once
#include "Engine/Global.h"
#include <string>
//スコアを管理するクラス
enum 
{
	RESULT_SCOREBOARD,		//スコア
	RESULT_HIGHSCOREBOARD,	//ハイスコア
	RESULT＿KEYGUIDE,		//ボタン案内
	BACK_GROUND,			//拝啓
	SCOREUPADATE,			//ハイスコア更新
	SCORE_IMAGE_MAX			
};

//スコアに使用するス数字(0-9)
enum SCORE_NUMBER
{
	SCORE_0,
	SCORE_1,
	SCORE_2,
	SCORE_3,
	SCORE_4,
	SCORE_5,
	SCORE_6,
	SCORE_7,
	SCORE_8,
	SCORE_9,
	SCORE_NUMBER_MAX
};

class Score : public IGameObject
{
	//画像の管理番号
	int hImage_[SCORE_IMAGE_MAX];

	//スコアナンバー表示用の画像配列
	int hNumImage_[SCORE_NUMBER_MAX];

	//スコア
	static unsigned int score_;

	//ハイスコア
	static unsigned int highScore_;

	//ハイスコア更新をしたかどうか
	bool isHighScoreUpdate_;

	//スコアを表示するかどうか
	bool scoreShowFlag_;

	//ハイスコアを表示するかどうか
	bool highScoreShowFlag_;

	//ハイスコア更新を表示するかどうか
	bool highScoreUpdateShowFlag_;

	//スコア表示音を鳴らすか同課のフラグ
	bool scoreShowSoundFlag_;

	//ハイスコア表示音を鳴らすかどうかのフラグ
	bool highScoreShowSoundFlag_;

public:
	//コンストラクタ
	Score(IGameObject* parent);

	//デストラクタ
	~Score();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//スコアを加算
	//引数	:	score 加算するスコアポイント
	//戻り値	:	なし
	void AddScore(unsigned int score);

	//スコアを表示
	//引数	:	なし
	//戻り値	:	なし
	void ShowScore();

	//スコアの読み取り
	//引数	:	スコアファイルの名前
	//戻り値	:	なし
	void ReadScoreFile(std::string fileName);

	//スコアを記録
	//引数	:	スコアファイルの名前
	//戻り値	:	なし
	void SaveScoreFile(std::string fileName);

	//ハイスコアを読み取り
	//引数	:	スコアファイルの名前
	//戻り値	:	なし
	static void ReadHighScoreFile(std::string fileName);

	//ハイスコアを記録
	//引数	:	スコアファイルの名前
	//戻り値	:	なし
	static void SaveHighScoreFile(std::string fileName);

	//ハイスコアを表示
	//引数	:	スコアファイルの名前
	//戻り値	:	なし
	void ShowHighScore();

	//スコアを表示するかしないかのフラグを決めるために使用
	//引数 :		スコアの表示の有無
	//戻り値	:	なし
	void SetScoreShowFlag(bool flag);

	//スコア音を鳴らすかのフラグを決めるために使用
	//引数 :		スコアの表示の有無
	//戻り値	:	なし
	void SetScoreShowSoundFlag(bool flag);

	//ハイスコアを表示するかしないかのフラグを決めるために使用
	//引数	:	ハイスコア表示の有無
	//戻り値	:	なし
	void SetHighScoreShowFlag(bool flag);

	//ハイスコア音を鳴らすかのフラグを決めるために使用
	//引数	:	ハイスコア表示の有無
	//戻り値	:	なし
	void SetHighScoreShowSoundFlag(bool flag);

	//ハイスコア更新表示するかしないかのフラグを決めるために使用
	//引数	:	ハイスコア更新表示の有無
	//戻り値	:	なし
	void SetHighScoreUpdateShowFlag(bool flag);
		
	//現在のスコア表示するかしないかのフラグを確認するために使用
	//引数	:	なし
	//戻り値	:	スコアを表示するかの有無
	bool GetScoreShowFlag();

	//現在のスコア表示音のフラグを確認するために使用
	//引数	:	なし
	//戻り値	:	スコアを表示音の有無
	bool GetScoreShowSoundFlag();

	//現在のハイスコア表示するかしないかのフラグを確認するために使用
	//引数	:	なし
	//戻り値	:	ハイスコア表示の有無
	bool GetHighScoreShowFlag();

	//現在のハイスコア表示するかしないかのフラグを確認するために使用
	//引数	:	なし
	//戻り値	:	ハイスコア表示音の有無
	bool GetHighScoreShowSoundFlag();

	//現在のハイスコア更新表示するかしないかのフラグを確認するために使用
	//引数	:	なし
	//戻り値	:	ハイスコア更新表示の有無
	bool GetHighScoreUpdateFlag();
};
