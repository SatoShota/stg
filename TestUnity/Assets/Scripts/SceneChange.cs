﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//シーン切り替えに使用するライブラリ

//シーンを切り替えるScript
//File>Build Settingsをクリックし、出てくるウィンドウのScenes in Buildに作成したスタート画面、ゲーム画面の２つのシーンファイルをドラッグ＆ドロッぷすることを忘れずに
public class SceneChange : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //spaceキーを押したならシーンを切り替える
        if (Input.GetKeyDown("space"))
        {
            //LoadSceneModeでシーンを切り替えることが可能
            //第二引数にはSingleとAdditiveがあって、それぞれ現在のシーンをアンロードする/しないに対応しています。
            //シングルのほうがデフォルトで校舎のほうはよくわからん
            switch(SceneManager.GetActiveScene().name)
            {
                case "PlayScene":
                    SceneManager.LoadScene("ResultScene", LoadSceneMode.Single);
                    break;
                case "TitleScene":
                    SceneManager.LoadScene("PlayScene", LoadSceneMode.Single);
                    break;
                case "ResultScene":
                    SceneManager.LoadScene("TitleScene", LoadSceneMode.Single);
                    break;
            }
           
        }
    }
}
